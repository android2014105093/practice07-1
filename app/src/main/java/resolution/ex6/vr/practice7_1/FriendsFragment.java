package resolution.ex6.vr.practice7_1;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class FriendsFragment extends Fragment {


    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends, container, false);

        ArrayList<String> friends = new ArrayList<String>();
        friends.add("KNU");
        friends.add("2016 MobileAppProgramming");
        friends.add("SK JUNG");
        friends.add("TEST LIST");

        ArrayAdapter<String> adpt = new ArrayAdapter<String>(this.getActivity(), R.layout.friends, friends);

        ListView lv = (ListView) view.findViewById(R.id.list_friends);
        lv.setAdapter(adpt);

        return view;
    }

}
